import React, { Component } from "react";
import Index from "./App/index";
import { AsyncStorage, Platform, PushNotificationIOS } from "react-native";
import firebase from "react-native-firebase";
import SplashScreen from "react-native-splash-screen";

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    SplashScreen.hide();
    this.checkPermission();
    this.createNotificationListeners();

    if (Platform.OS === "ios") {
      PushNotificationIOS.addEventListener("register", token => {
        console.log("IOS tokan ", token);
      });
      PushNotificationIOS.requestPermissions();
    }
    this.onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh(fcmToken => {
        console.log("LOG: ", fcmToken);
      });
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      console.log("Notification permission allow");
      this.getToken();
    } else {
      this.requestPermission();
      console.log("Notification permission not allow");
    }
  }
  async getToken() {
    let fcmToken = await AsyncStorage.getItem("androidToken");
    console.log(fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log("androidToken", fcmToken);
        await AsyncStorage.setItem("androidToken", fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();

      this.getToken();
    } catch (error) {
      console.log("permission rejected");
    }
  }
  componentWillUnmount() {
    this.unsubscribeFromNotificationListener();
    this.notificationOpenedListener();
  }
  async createNotificationListeners() {
    const channel = new firebase.notifications.Android.Channel(
      "com.mysafetynetadult",
      "notification",
      firebase.notifications.Android.Importance.Max
    ).setDescription("A natural description of the channel");
    firebase.notifications().android.createChannel(channel);

    const channelGroup = new firebase.notifications.Android.ChannelGroup(
      "com.mysafetynetadult",
      "notification"
    );
    firebase.notifications().android.createChannelGroup(channelGroup);

    this.unsubscribeFromNotificationListener = firebase
      .notifications()
      .onNotification(notification => {
        if (Platform.OS === "android") {
          console.log(notification.data);

          const localNotification = new firebase.notifications.Notification({
            sound: "default",
            show_in_foreground: true
          })
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .android.setChannelId("com.mysafetynetadult")
            .android.setSmallIcon("ic_notification")
            .android.setAutoCancel(true)
            .android.setGroupSummary(true)
            .android.setGroup("com.mysafetynetadult")
            .android.setPriority(firebase.notifications.Android.Priority.High);

          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
        } else if (Platform.OS === "ios") {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .ios.setBadge(notification.ios.badge);
          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));
          console.log("localNotification: ", localNotification);
        }
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen.notification;
        if (notificationOpen) {
          const action = notificationOpen.action;
          const notification = notificationOpen.notification;
          firebase
            .notifications()
            .removeDeliveredNotification(notification.notificationId);
          console.log("If your app is in background, : ", notification);
          AsyncStorage.setItem(
            "notification",
            JSON.stringify(notification.data)
          );
          if (Platform.OS === "android") {
            AsyncStorage.setItem(
              "notification",
              JSON.stringify(notification.data)
            );
            console.log("android custom data: ", notification.data.extra);
          } else {
            AsyncStorage.setItem(
              "notification",
              JSON.stringify(notification.data)
            );
            console.log("ios custom data: ", notification.data.extra);
          }
        }
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */

    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          const action = notificationOpen.action;
          const notification = notificationOpen.notification;

          AsyncStorage.setItem(
            "notification",
            JSON.stringify(notification.data)
          );

          console.log("Notification", notification.data);
          if (Platform.OS === "android") {
            console.log(
              "android custom data close app: ",
              notification.data.extra
            );
          } else {
            console.log("ios custom data close app: ", notification.data.extra);
          }
        }
      });
  }

  render() {
    return <Index />;
  }
}
