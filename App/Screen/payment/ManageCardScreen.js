import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Header, Icon, Text, Button, Image } from "react-native-elements";
import Colors from "../Resource/Colors";
import Icons from "../Resource/Icons";
import ManageCardScreenStyle from "../Resource/Style/ManageCardScreenStyle";

class ManageCardScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 180 }}
            >
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image
                  source={Icons.left_arrow}
                  style={{ width: 24, height: 24,marginStart:10 }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  color: Colors.black,
                  fontSize: 16,
                  marginStart: 10,
                  fontFamily: "AktivGrotesk-Bold"
                }}
              >
                MANAGE CARDS
              </Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      cardsData: [
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" },
        { number: "454545", month: "12/28", cvv: "***" }
      ]
    };
   
  }
  
  doRedirect(screen) {
    this.props.navigation.navigate(screen);
  }
  renderOnRefresh() {}
  renderItem(item, index) {
    return (
      <View
        style={{
          backgroundColor: Colors.white,
          flexDirection: "row",
          borderRadius: 4,
          marginTop: 5,
          marginBottom: 5,
          alignItems: "center"
        }}
      >
        <Text
          style={{
            flex: 1,
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 16,
            color: Colors.black,
            marginStart: 15,
            marginTop: 15,
            marginBottom: 15
          }}
        >
          {item.number}
        </Text>
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 16,
            color: Colors.black,
            marginTop: 15,
            marginBottom: 15
          }}
        >
          {item.month}
        </Text>
        <TouchableOpacity>
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 16,
            color: '#2640DF',
            marginStart: 15,
            marginEnd: 15,
            marginTop: 15,
            marginBottom: 15
          }}
        >
          Edit
        </Text>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    return (
      <SafeAreaView style={ManageCardScreenStyle.container}>
        <View style={ManageCardScreenStyle.container}>
          <Image
            source={Icons.bg_cart}
            style={ManageCardScreenStyle.checked}
            resizeMode="contain"
          />
          <View
            style={{ flexDirection: "row", marginStart: "5%", marginEnd: "5%" }}
          >
            <Text
              style={{
                flex: 1,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: 16,
                color: Colors.black
              }}
            >
              Card
            </Text>
           
          </View>

          <ActivityIndicator
            size="small"
            style={{
              display: this.state.refreshing ? "flex" : "none",
              margin: "10%"
            }}
          />
          <FlatList
            onRefresh={() => this.renderOnRefresh()}
            data={this.state.cardsData}
            renderItem={({ item, index }) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.refreshing}
            style={{
              marginStart: "5%",
              marginEnd: "5%",
              display: this.state.refreshing ? "none" : "flex",
              marginBottom: "20%"
            }}
          />
          <View style={ManageCardScreenStyle.view_button}>
            <Button
              loading={this.state.loading}
              title="UPDATE"
             
              buttonStyle={{
                backgroundColor: "#3444D6",
                borderRadius: 20,
                alignItems: "center"
              }}
              titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
export default ManageCardScreen;
