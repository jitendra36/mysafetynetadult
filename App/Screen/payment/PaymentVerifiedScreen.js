import React, { PureComponent } from "react";
import { View, SafeAreaView } from "react-native";
import { Header, Icon, Text, Button, Image } from "react-native-elements";
import Colors from "../Resource/Colors";
import Icons from "../Resource/Icons";
import PaymentVerifiedScreenStyle from "../Resource/Style/PaymentVerifiedScreenStyle";
class PaymentVerifiedScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 180 }}
            >
              {/*  <Icon
                name="arrow-back"
                size={35}
                color="#454F63"
                onPress={() => navigation.goBack()}
              /> */}
              <Text
                style={{
                  color: Colors.black,
                  fontSize: 16,
                  marginStart: 10,
                  fontFamily: "AktivGrotesk-Bold"
                }}
              >
                PAYMENT VERIFIED
              </Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  render() {
    return (
      <SafeAreaView style={PaymentVerifiedScreenStyle.container}>
        <View style={PaymentVerifiedScreenStyle.container}>
          <View style={PaymentVerifiedScreenStyle.card}>
            <Image
              source={Icons.checked}
              style={PaymentVerifiedScreenStyle.checked}
            />
            <Text style={PaymentVerifiedScreenStyle.text_success}>
              Your payment made has been successfully
            </Text>
          </View>
          <View style={PaymentVerifiedScreenStyle.view_button}>
            <Button
              loading={this.state.loading}
              title="VERIFY YOUR PHONE NUMBER"
              iconRight={true}
              icon={<Image source={Icons.right_arrow} style={{height:20,width:20,tintColor:Colors.white,marginStart:10}}/>}
              buttonStyle={{
                backgroundColor: "#3444D6",
                borderRadius: 20,
                alignItems: "center"
              }}
              titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
export default PaymentVerifiedScreen;
