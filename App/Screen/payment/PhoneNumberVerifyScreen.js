import React, { PureComponent } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import { Header, Text, Button, Image } from "react-native-elements";
import Colors from "../Resource/Colors";
import Icons from "../Resource/Icons";
import PhoneNumberVerifyScreenStyle from "../Resource/Style/PhoneNumberVerifyScreenStyle";
class PhoneNumberVerifyScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 220 }}
            >
              {/*  <Icon
                name="arrow-back"
                size={35}
                color="#454F63"
                onPress={() => navigation.goBack()}
              /> */}
              <Text
                style={{
                  color: Colors.black,
                  fontSize: 16,
                  marginStart: 10,
                  fontFamily: "AktivGrotesk-Bold"
                }}
              >
                CONFIRM YOUR IDENTITY
              </Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  render() {
    return (
      <SafeAreaView style={PhoneNumberVerifyScreenStyle.container}>
        <ScrollView style={PhoneNumberVerifyScreenStyle.container}>
          <View style={PhoneNumberVerifyScreenStyle.container}>
            <View style={PhoneNumberVerifyScreenStyle.card}>
              <Image
                source={Icons.phonecall}
                style={PhoneNumberVerifyScreenStyle.checked}
              />
              <Text style={PhoneNumberVerifyScreenStyle.text_in_progress}>
                Call in Progress...
              </Text>
              <Text style={PhoneNumberVerifyScreenStyle.text_success}>
                Please answer the call from and when promoted, enter the 4-digit
                number your phone keypad.
              </Text>
              <Text style={PhoneNumberVerifyScreenStyle.text_callon}>
                Call on mobile number
              </Text>
              <Text style={PhoneNumberVerifyScreenStyle.text_mobile}>
                +91 846 078 5825
              </Text>

              <Text style={PhoneNumberVerifyScreenStyle.text_callon}>
                Please note down 4-digit PIN
              </Text>
              <View style={{ flexDirection: "row", marginBottom: "15%" }}>
                <View
                  style={[
                    PhoneNumberVerifyScreenStyle.view_pin,
                    { marginEnd: 5 }
                  ]}
                >
                  <Text style={PhoneNumberVerifyScreenStyle.text_pin}>1</Text>
                </View>
                <View
                  style={[
                    PhoneNumberVerifyScreenStyle.view_pin,
                    { marginStart: 5, marginEnd: 5 }
                  ]}
                >
                  <Text style={PhoneNumberVerifyScreenStyle.text_pin}>2</Text>
                </View>
                <View
                  style={[
                    PhoneNumberVerifyScreenStyle.view_pin,
                    { marginStart: 5, marginEnd: 5 }
                  ]}
                >
                  <Text style={PhoneNumberVerifyScreenStyle.text_pin}>3</Text>
                </View>
                <View
                  style={[
                    PhoneNumberVerifyScreenStyle.view_pin,
                    { marginStart: 5 }
                  ]}
                >
                  <Text style={PhoneNumberVerifyScreenStyle.text_pin}>4</Text>
                </View>
              </View>
            </View>
            <Text style={PhoneNumberVerifyScreenStyle.text_retry}>
              We are having difficulty contacting you or verifying the PIN you
              entered. You can retry the automated phone verification process or
              contact customer service to speak with a representative.
            </Text>
            <View style={PhoneNumberVerifyScreenStyle.view_button}>
              <Button
                loading={this.state.loading}
                title="CALL ME"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                containerStyle={{ marginTop: "10%",marginBottom:'10%' }}
                titleStyle={{
                  fontFamily: "AktivGrotesk-Bold",
                  fontSize: 16
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default PhoneNumberVerifyScreen;
