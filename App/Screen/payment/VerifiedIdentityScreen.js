import React, { PureComponent } from "react";
import { View, SafeAreaView } from "react-native";
import { Header, Icon, Text, Button, Image } from "react-native-elements";
import Colors from "../Resource/Colors";
import Icons from "../Resource/Icons";
import VerifiedIdentityScreenStyle from "../Resource/Style/VerifiedIdentityScreenStyle";
class VerifiedIdentityScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 220 }}
            >
              {/*  <Icon
                name="arrow-back"
                size={35}
                color="#454F63"
                onPress={() => navigation.goBack()}
              /> */}
              <Text
                style={{
                  color: Colors.black,
                  fontSize: 16,
                  marginStart: 10,
                  fontFamily: "AktivGrotesk-Bold"
                }}
              >
                VERIFIED YOUR IDENTITY
              </Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      timer: 10
    };
  }
  componentDidMount() {
    this.interval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000
    );
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      //  this.props.navigation.navigate('Login');
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
  render() {
    return (
      <SafeAreaView style={VerifiedIdentityScreenStyle.container}>
        <View style={VerifiedIdentityScreenStyle.container}>
          <View style={VerifiedIdentityScreenStyle.card}>
            <Image
              source={Icons.checked}
              style={VerifiedIdentityScreenStyle.checked}
            />
            <Text style={VerifiedIdentityScreenStyle.text_success}>
              Your identity has been verified successfully
            </Text>
          </View>
          <View style={VerifiedIdentityScreenStyle.view_button}>
            <Button
              loading={this.state.loading}
              title="CONTINUE"
              iconRight={true}
              icon={<Image source={Icons.right_arrow} style={{height:20,width:20,tintColor:Colors.white,marginStart:10}}/>}
              buttonStyle={{
                backgroundColor: "#3444D6",
                borderRadius: 20,
                alignItems: "center"
              }}
              titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{ width: 100, height: 1, backgroundColor: "#666666" }}
            />
            <Text style={VerifiedIdentityScreenStyle.text_or}>OR</Text>
            <View
              style={{ width: 100, height: 1, backgroundColor: "#666666" }}
            />
          </View>
          <Text style={VerifiedIdentityScreenStyle.text_redirected}>
            {" "}
            You will be redirected in {this.state.timer} seconds{" "}
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}
export default VerifiedIdentityScreen;
