import React, { PureComponent } from "react";
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import SignUpScreenStyle from "../../CustomStyle/index";
import {
  Header,
  Icon,
  Text,
  Button,
  CheckBox,
  Image
} from "react-native-elements";
import Icons from "../../Resource/index";
import { TextField } from "react-native-material-textfield";
import ImagePicker from "react-native-image-crop-picker";
import { Dropdown } from "react-native-material-dropdown";
import colors from "../../Resource/Color/index";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";

class RegistrationScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
          <Header
            statusBarProps={{ translucent: true }}
            containerStyle={{
              padding:0,
              backgroundColor: colors.white,
              justifyContent: "space-around"
            }}
            leftComponent={
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: 150,
                  flex: 1,
                 
                }}
              >
                <Icon
                  name="arrow-back"
                  size={30}
                  color="#454F63"
                  onPress={() => navigation.goBack()}
                />
                <Text style={SignUpScreenStyle.text_reg}>REGISTRATION</Text>
              </View>
            }
          />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      cbTerms: false,
      profilePicture: "",
      fName: "",
      lName: "",
      countryCode: "+1",
      phoneNumber: "",
      email: "",
      city: "",
      stateName: "",
      password: "",
      cpassword: "",
      errFName: "",
      errLName: "",
      errPhone: "",
      errEmail: "",
      errCity: "",
      errStateName: "",
      errPassword: "",
      errCPassword: "",
      countryCodes: []
    };
  }
  componentDidMount() {
    // this.getCountryCodes();
  }
  onTextChange(text) {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "" : "",
        number = [
          intlCode,
          "",
          match[2],
          " - ",
          match[3],
          " - ",
          match[4]
        ].join("");

      this.setState({ phoneNumber: number });

      return;
    }
    // this.setState({ phoneNumber: number });
  }
  pickSingle(cropit, circular = false, mediaType) {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: "MediumQuality",
      includeExif: true
    })
      .then(image => {
        console.log("received image", image);
        this.setState({ profilePicture: image.path });
      })
      .catch(e => {
        console.log(e);
      });
  }
  async getCountryCodes() {
    try {
      let response = await fetch("http://country.io/phone.json");
      let responseJson = await response.json();
      const tempData = [];
      Object.keys(responseJson).reduce((result, currentKey) => {
        console.log();
        const code = {
          value: responseJson[currentKey]
        };
        tempData.push(code);
      }, []);
      const uniqueNames = Array.from(new Set(tempData));

      this.setState({ countryCodes: uniqueNames });
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }
  doRegistration() {
    const {
      profilePicture,
      fName,
      lName,
      phoneNumber,
      email,
      city,
      stateName,
      password,
      cpassword,
      cbTerms
    } = this.state;

    if (profilePicture === "") {
      alert(string.errImage);
    } else if (fName === "") {
      this.fNameRef.focus();
      this.setState({ errFName: string.errFName });
    } else if (lName === "") {
      this.lNameRef.focus();
      this.setState({ errLName: string.errLName });
    } else if (phoneNumber === "") {
      this.phoneRef.focus();
      this.setState({ errPhone: string.errPhone });
    } else if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: string.errEmail });
    } else if (!this.validate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: string.errEmail });
    } else if (city === "") {
      this.cityRef.focus();
      this.setState({ errCity: string.errCity });
    } else if (stateName === "") {
      this.stateRef.focus();
      this.setState({ errStateName: string.errStateName });
    } else if (password === "") {
      this.passRef.focus();
      this.setState({ errPassword: string.errPassword });
    } else if (password.length <= 4) {
      this.passRef.focus();
      this.setState({ errPassword: string.errPasswordLen });
    } else if (cpassword === "") {
      this.cpassRef.focus();
      this.setState({ errCPassword: string.errCPassword });
    } else if (cpassword.length <= 4) {
      this.cpassRef.focus();
      this.setState({ errCPassword: string.errPasswordLen });
    } else if (password != cpassword) {
      this.cpassRef.focus();
      this.setState({ errCPassword: string.errCPasswordMatch });
    } else if (!cbTerms) {
      alert(string.errTerms);
    }
  }
  validate(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    return (
      <SafeAreaView
        style={[
          SignUpScreenStyle.container,
          { backgroundColor: colors.bg_color }
        ]}
      >
        <ScrollView style={SignUpScreenStyle.container}>
          <View style={SignUpScreenStyle.container}>
            <View style={SignUpScreenStyle.topView}>
              <View style={SignUpScreenStyle.cartView}>
                <View style={SignUpScreenStyle.card}>
                  <TextField
                    ref={ref => {
                      this.fNameRef = ref;
                    }}
                    value={this.state.fName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ fName: text });
                    }}
                    onSubmitEditing={() => {
                      this.lNameRef.focus();
                    }}
                    returnKeyType="next"
                    label="First Name"
                    error={this.state.errFName}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    underlineColorAndroid={colors.transparent}
                    tintColor={colors.inputTextColor}
                    fontSize={16}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    containerStyle={[
                      SignUpScreenStyle.signContainerStyle,
                      { paddingTop: "28%" }
                    ]}
                    onBlur={() => this.setState({ errFName: "" })}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <TextField
                    ref={ref => {
                      this.lNameRef = ref;
                    }}
                    value={this.state.lName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ lName: text });
                    }}
                    onSubmitEditing={() => {
                      this.phoneRef.focus();
                    }}
                    returnKeyType="next"
                    label="Last Name"
                    error={this.state.errLName}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    underlineColorAndroid={colors.transparent}
                    tintColor={colors.inputTextColor}
                    fontSize={16}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    onBlur={() => this.setState({ errLName: "" })}
                    containerStyle={SignUpScreenStyle.signContainerStyle}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <View style={SignUpScreenStyle.signDropDownView}>
                    <View style={{ flex: 1 }}>
                      <Dropdown
                        data={this.state.countryCodes}
                        containerStyle={{ width: 80, paddingRight: 5 }}
                        value={this.state.countryCode}
                        onChangeText={(value, index, data) =>
                          this.setState({ countryCode: value })
                        }
                      />
                    </View>
                    <View style={{ flex: 3 }}>
                      <TextField
                        ref={ref => {
                          this.phoneRef = ref;
                        }}
                        value={this.state.phoneNumber}
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        onChangeText={text => this.onTextChange(text)}
                        onSubmitEditing={() => {
                          this.emailRef.focus();
                        }}
                        returnKeyType="next"
                        label="Phone Number"
                        keyboardType="number-pad"
                        textContentType="telephoneNumber"
                        dataDetectorTypes="phoneNumber"
                        error={this.state.errPhone}
                        maxLength={16}
                        placeholderTextColor={colors.inputTextColor}
                        textColor={colors.inputTextColor}
                        underlineColorAndroid={colors.transparent}
                        tintColor={colors.inputTextColor}
                        fontSize={16}
                        inputContainerStyle={
                          SignUpScreenStyle.inputContainerStyleTextField
                        }
                        containerStyle={[
                          SignUpScreenStyle.signContainerStyle,
                          { marginStart: 10 }
                        ]}
                        onBlur={() => this.setState({ errPhone: "" })}
                        labelTextStyle={{ fontFamily: font.Regular }}
                        titleTextStyle={{ fontFamily: font.Regular }}
                        affixTextStyle={{ fontFamily: font.Regular }}
                      />
                    </View>
                  </View>
                  <TextField
                    ref={ref => {
                      this.emailRef = ref;
                    }}
                    value={this.state.email}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ email: text });
                    }}
                    onSubmitEditing={() => {
                      this.cityRef.focus();
                    }}
                    returnKeyType="next"
                    label="Email"
                    keyboardType="email-address"
                    error={this.state.errEmail}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    underlineColorAndroid={colors.transparent}
                    tintColor={colors.inputTextColor}
                    fontSize={16}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errEmail: "" })}
                    containerStyle={SignUpScreenStyle.signContainerStyle}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <TextField
                    ref={ref => {
                      this.cityRef = ref;
                    }}
                    value={this.state.city}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ city: text });
                    }}
                    onSubmitEditing={() => {
                      this.stateRef.focus();
                    }}
                    returnKeyType="next"
                    label="City"
                    error={this.state.errCity}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    underlineColorAndroid={colors.transparent}
                    tintColor={colors.inputTextColor}
                    fontSize={16}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errCity: "" })}
                    containerStyle={SignUpScreenStyle.signContainerStyle}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <TextField
                    ref={ref => {
                      this.stateRef = ref;
                    }}
                    value={this.state.stateName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ stateName: text });
                    }}
                    onSubmitEditing={() => {
                      this.passRef.focus();
                    }}
                    returnKeyType="next"
                    label="State"
                    error={this.state.errStateName}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    underlineColorAndroid={colors.transparent}
                    tintColor={colors.inputTextColor}
                    fontSize={16}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    onBlur={() => this.setState({ errStateName: "" })}
                    containerStyle={SignUpScreenStyle.signContainerStyle}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <TextField
                    tintColor={colors.inputTextColor}
                    ref={ref => {
                      this.passRef = ref;
                    }}
                    value={this.state.password}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ password: text });
                    }}
                    onSubmitEditing={() => {
                      this.cpassRef.focus();
                    }}
                    returnKeyType="next"
                    label="Password"
                    error={this.state.errPassword}
                    fontSize={16}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    secureTextEntry={true}
                    underlineColorAndroid={colors.transparent}
                    inputContainerStyle={
                      SignUpScreenStyle.inputContainerStyleTextField
                    }
                    onBlur={() => this.setState({ errPassword: "" })}
                    containerStyle={SignUpScreenStyle.signContainerStyle}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                  <TextField
                    tintColor={colors.inputTextColor}
                    ref={ref => {
                      this.cpassRef = ref;
                    }}
                    value={this.state.cpassword}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ cpassword: text });
                    }}
                    returnKeyType="done"
                    label="Confirm Password"
                    error={this.state.errCPassword}
                    fontSize={16}
                    placeholderTextColor={colors.inputTextColor}
                    textColor={colors.inputTextColor}
                    secureTextEntry={true}
                    underlineColorAndroid={colors.transparent}
                    inputContainerStyle={[
                      SignUpScreenStyle.inputContainerStyleTextField
                    ]}
                    onBlur={() => this.setState({ errCPassword: "" })}
                    containerStyle={SignUpScreenStyle.conformPasswordView}
                    labelTextStyle={{ fontFamily: font.Regular }}
                    titleTextStyle={{ fontFamily: font.Regular }}
                    affixTextStyle={{ fontFamily: font.Regular }}
                  />
                </View>
                <CheckBox
                  title="Terms and conditions"
                  checked={this.state.cbTerms}
                  onPress={() =>
                    this.setState({ cbTerms: !this.state.cbTerms })
                  }
                  checkedIcon="check-square"
                  size={24}
                  containerStyle={SignUpScreenStyle.checkBoxView}
                  textStyle={{ fontFamily: font.Regular }}
                  checkedColor="#3444D6"
                />
                <Button
                  loading={this.state.loading}
                  title="NEXT"
                  iconRight={true}
                  icon={<Icon name="arrow-forward" size={20} color="white" />}
                  buttonStyle={SignUpScreenStyle.signUpButton}
                  containerStyle={{
                    marginBottom: 20,
                    marginStart: 20,
                    marginEnd: 20
                  }}
                  onPress={() => this.doRegistration()}
                  titleStyle={{ fontFamily: font.Regular }}
                />
              </View>
              <View style={SignUpScreenStyle.roundImage}>
                <Image
                  source={
                    this.state.profilePicture == ""
                      ? Icons.user
                      : { uri: this.state.profilePicture }
                  }
                  style={SignUpScreenStyle.imageStyle}
                  resizeMethod="auto"
                  resizeMode="contain"
                  PlaceholderContent={<ActivityIndicator />}
                />
                <TouchableOpacity
                  style={SignUpScreenStyle.upload}
                  onPress={() => this.pickSingle(true)}
                >
                  <Text style={SignUpScreenStyle.uploadText}>Upload</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default RegistrationScreen;
