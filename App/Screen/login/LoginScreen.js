import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView,AsyncStorage
} from "react-native";
import { Text, Button, Image } from "react-native-elements";
import LoginScreenStyle from "../../CustomStyle/index";
import { TextField } from "react-native-material-textfield";
import Icons from "../../Resource/index";
import colors from "../../Resource/Color";
import font from "../../Resource/fontFamily";
import string from "../../Resource/string";

class LoginScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      errEmail: "",
      password: "",
      errPassword: ""
    };
  }
  static navigationOptions = {
    header: null
  };
  doRediect(screen) {
    this.props.navigation.navigate(screen);
  }

  getData(){
    setTimeout(() => {
      AsyncStorage.getItem("notification")
      .then(data => {
        console.log("data:",data);
        const myData = JSON.parse(data);
        console.log("custom:",myData);
        
        if (myData != null) {
            if (myData.extra==="notification") {
              this.doRediect("Notification");
              AsyncStorage.removeItem("notification");
            } else {
              
            }
        } else {
          console.log("sorry data null");
        }
      })
      .done(); 
    }, 500);
  }
  componentWillMount() {
    this.getData();

  }
  doLogin() {
    const { email, password } = this.state;
    this.props.navigation.navigate("Tabs");
    console.log(password);
    if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: string.emptyEmail });
    } else if (!this.validate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: string.invalidEmail });
    } else if (password === "") {
      this.passRef.focus();
      this.setState({ errPassword: string.emptyPassword });
    } else if (password.length <= 4) {
      this.passRef.focus();
      this.setState({ errPassword: string.invalidPassword });
    } else {
      this.setState({ errEmail: "", errPassword: "" });
    }
  }
  validate(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    return (
      <SafeAreaView style={LoginScreenStyle.container}>
        <ImageBackground
          source={Icons.bg_login}
          style={LoginScreenStyle.ImageBackground}
        >
          <ScrollView>
            <View style={LoginScreenStyle.container}>
              <Image
                source={Icons.badge_white}
                style={LoginScreenStyle.image_badge}
              />
              <View style={LoginScreenStyle.card}>
                <Text style={LoginScreenStyle.text_login}>{string.login}</Text>
                <TextField
                  ref={ref => {
                    this.emailRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.email}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}
                  onSubmitEditing={() => {
                    this.passRef.focus();
                  }}
                  returnKeyType="next"
                  label="Email"
                  keyboardType="email-address"
                  error={this.state.errEmail}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  fontSize={16}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  autoCapitalize="none"
                  onBlur={() => this.setState({ errEmail: "" })}
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                />
                <TextField
                  ref={ref => {
                    this.passRef = ref;
                  }}
                  underlineColorAndroid={colors.transparent}
                  value={this.state.password}
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onChangeText={text => {
                    this.setState({ password: text });
                  }}
                  returnKeyType="done"
                  label="Password"
                  error={this.state.errPassword}
                  fontSize={16}
                  placeholderTextColor={colors.inputTextColor}
                  textColor={colors.inputTextColor}
                  tintColor={colors.inputTextColor}
                  secureTextEntry={true}
                  containerStyle={LoginScreenStyle.containerStyleTextField}
                  inputContainerStyle={
                    LoginScreenStyle.inputContainerStyleTextField
                  }
                  labelTextStyle={{ fontFamily: font.Regular }}
                  titleTextStyle={{ fontFamily: font.Regular }}
                  affixTextStyle={{ fontFamily: font.Regular }}
                  onBlur={() => this.setState({ errPassword: "" })}
                />
                <Button
                  loading={this.state.loading}
                  title="LOGIN"
                  buttonStyle={LoginScreenStyle.loginButton}
                  containerStyle={LoginScreenStyle.loginButtonContainer}
                  onPress={() => this.doLogin()}
                  titleStyle={{ fontFamily: font.Regular }}
                />
              </View>

              <TouchableOpacity>
                <Text style={LoginScreenStyle.text_forgot}>
                  {string.forgotPassword}
                </Text>
              </TouchableOpacity>
              <Button
                title={string.signUp}
                buttonStyle={LoginScreenStyle.signUpButton}
                containerStyle={LoginScreenStyle.button_signup}
                onPress={() => this.doRediect("SignUp")}
                titleStyle={{ fontFamily: font.Regular }}
              />
            </View>
          </ScrollView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

export default LoginScreen;
