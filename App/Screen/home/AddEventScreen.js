import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator
} from "react-native";
import Colors from "../Resource/Colors";
import { Header,  Text, Image, Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import Icons from "../Resource/Icons";
import moment from "moment";
import DateTimePicker from "react-native-modal-datetime-picker";

class AddEventScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                width: 130
              }}
            >
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image
                  source={Icons.left_arrow}
                  style={{ width: 24, height: 24,marginStart:10 }}
                />
              </TouchableOpacity>

              <Text
                style={{
                  color: Colors.black,
                  fontSize: 16,
                  marginStart: 10,
                  fontFamily: "AktivGrotesk-Bold"
                }}
              >
                ADD EVENT
              </Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      loading: false,
      refreshing: false,
      isDateTimePickerVisible: false,
      eventTitle: "Untitled Event",
      errEventTitle: "",
      eventStartDate: moment(new Date()).format("ddd D MMM, YYYY"),
      eventEndDate: "Event End Date",
      eventStartTime: moment(new Date()).format("hh:mm A"),
      eventEndTime: "Event End Time",
      invitedUsers: [
        {
          name: "Smith Clarrt",
          image:
            "http://ap.church/wp-content/uploads/2018/08/Brian-Jones-Profile.jpg"
        }
      ]
    };
  }
  _showDateTimePicker = index => {
    this.setState({ index: index, isDateTimePickerVisible: true });
  };

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    switch (this.state.index) {
      case 1:
        this.setState({
          eventStartDate: moment(date).format("ddd D MMM, YYYY")
        });
        break;
      case 2:
        this.setState({
          eventStartTime: moment(date).format("hh:mm A")
        });
        break;
      case 3:
        this.setState({
          eventEndDate: moment(date).format("ddd D MMM, YYYY")
        });
        break;
      case 4:
        this.setState({
          eventEndTime: moment(date).format("hh:mm A")
        });
        break;
      default:
        break;
    }
    this._hideDateTimePicker();
  };
  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: 100, height: 100, borderRadius: 4 }}
          PlaceholderContent={<ActivityIndicator/>}
        />

        <Text
          style={{
            marginTop: 5,
            color: "#666666",
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 13,
            textAlign: "center"
          }}
        >
          {item.name}
        </Text>
      </View>
    );
  }
  renderAddInvite() {
    return (
      <View
        style={{
          borderRadius: 4,
          width: 100,
          height: 100,
          backgroundColor: "#D6D6D6",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image source={Icons.invite_user} style={{ width: 100, height: 100 }} />
        <TouchableOpacity
          style={{ position: "absolute", bottom: 0, marginBottom: 10 }}
        >
          <Text
            style={{
              color: "#666666",
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 13,
              textAlign: "center"
            }}
          >
            + Add Invite
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
        <ScrollView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%"
              }}
            >
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventTitle}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventTitle: text });
                }}
                returnKeyType="next"
                label="EVENT TITLE"
                error={this.state.errEventTitle}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={25}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: Colors.transparent,
                  borderBottomWidth: 0
                }}
                onBlur={() => this.setState({ errEventTitle: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                EVENT TIME
              </Text>
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                mode={
                  this.state.index == 1
                    ? "date"
                    : this.state.index == 3
                    ? "date"
                    : "time"
                }
              />
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  onPress={() => this._showDateTimePicker(1)}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventStartDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  onPress={() => this._showDateTimePicker(2)}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventStartTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  onPress={() => this._showDateTimePicker(3)}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventEndDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  onPress={() => this._showDateTimePicker(4)}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventEndTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30
                }}
              >
                LOCATION
              </Text>
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventLocation}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventLocation: text });
                }}
                returnKeyType="next"
                label="EVENT LOCATION"
                error={this.state.errEventLocation}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={13}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventLocation: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30,
                  marginBottom: 10
                }}
              >
                INVITE
              </Text>
              <View>
                <FlatList
                  horizontal={true}
                  data={this.state.invitedUsers}
                  renderItem={({ item, index }) =>
                    this.renderInvite(item, index)
                  }
                  ListHeaderComponent={() => this.renderAddInvite()}
                  keyExtractor={(item, index) => index.toString()}
                  refreshing={this.state.refreshing}
                  style={{ marginStart: 10, marginEnd: 10, marginBottom: 20 }}
                />
              </View>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30
                }}
              >
                NOTES
              </Text>
              <TextField
                ref={ref => {
                  this.eventNotesRef = ref;
                }}
                value={this.state.eventNotes}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventNotes: text });
                }}
                returnKeyType="next"
                label="NOTES"
                error={this.state.errEventNotes}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={13}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventNotes: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
            </View>
            <View
              style={{
                margin: 15
              }}
            >
              <Button
                loading={this.state.loading}
                title="SAVE"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                titleStyle={{
                  fontFamily: "AktivGrotesk-Bold",
                  fontSize: 16
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default AddEventScreen;
