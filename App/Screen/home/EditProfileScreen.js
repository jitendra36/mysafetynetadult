import React, { PureComponent } from "react";
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import SignUpScreenStyle from "../Resource/Style/SignUpScreenStyle";
import {
  Header,
  
  Text,
  Button,
  
  Image
} from "react-native-elements";
import Colors from "../Resource/Colors";
import Icons from "../Resource/Icons";
import { TextField } from "react-native-material-textfield";
import ImagePicker from "react-native-image-crop-picker";
import { Dropdown } from "react-native-material-dropdown";

class EditProfileScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <Header
          statusBarProps={{ translucent: true }}
          containerStyle={{
            backgroundColor: Colors.white,
            justifyContent: "space-around"
          }}
          leftComponent={
            <View
              style={{ flexDirection: "row", alignItems: "center", width: 130 }}
            >
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image
                  source={Icons.left_arrow}
                  style={{ width: 24, height: 24 ,marginStart:10}}
                />
              </TouchableOpacity>
              <Text style={SignUpScreenStyle.text_reg}>EDIT PROFILE</Text>
            </View>
          }
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      profilePicture: "",
      fName: "",
      lName: "",
      countryCode: "+1",
      phoneNumber: "",
      email: "",
      city: "",
      stateName: "",
      errFName: "",
      errLName: "",
      errPhone: "",
      errEmail: "",
      errCity: "",
      errStateName: "",

      countryCodes: []
    };
  }
  componentDidMount() {
    this.getCountryCodes();
  }
  onTextChange(text) {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "" : "",
        number = [
          intlCode,
          "",
          match[2],
          " - ",
          match[3],
          " - ",
          match[4]
        ].join("");

      this.setState({ phoneNumber: number });

      return;
    }
    // this.setState({ phoneNumber: number });
  }
  pickSingle(cropit, circular = false, mediaType) {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: "MediumQuality",
      includeExif: true
    })
      .then(image => {
        console.log("received image", image);
        this.setState({ profilePicture: image.path });
      })
      .catch(e => {
        console.log(e);
      });
  }
  async getCountryCodes() {
    try {
      let response = await fetch("http://country.io/phone.json");
      let responseJson = await response.json();
      const tempData = [];
      Object.keys(responseJson).reduce((result, currentKey) => {
        console.log();
        const code = {
          value: responseJson[currentKey]
        };
        tempData.push(code);
      }, []);
      const uniqueNames = Array.from(new Set(tempData));

      this.setState({ countryCodes: uniqueNames });
      return responseJson;
    } catch (error) {
      console.error(error);
    }
  }
  doRegistration() {
    const {
      profilePicture,
      fName,
      lName,
      phoneNumber,
      email,
      city,
      stateName
    } = this.state;

    if (profilePicture === "") {
      alert("Please select Profile Picture");
    } else if (fName === "") {
      this.fNameRef.focus();
      this.setState({ errFName: "Please enter First Name" });
    } else if (lName === "") {
      this.lNameRef.focus();
      this.setState({ errLName: "Please enter Last Name" });
    } else if (phoneNumber === "") {
      this.phoneRef.focus();
      this.setState({ errPhone: "Please enter Phone Number" });
    } else if (email === "") {
      this.emailRef.focus();
      this.setState({ errEmail: "Please enter Email" });
    } else if (!this.validate(email)) {
      this.emailRef.focus();
      this.setState({ errEmail: "Please enter valid Email" });
    } else if (city === "") {
      this.cityRef.focus();
      this.setState({ errCity: "Please enter City" });
    } else if (stateName === "") {
      this.stateRef.focus();
      this.setState({ errStateName: "Please enter State" });
    } else {
      this.setState({ errStateName: "" });
    }
  }
  validate(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    return (
      <SafeAreaView style={SignUpScreenStyle.container}>
        <ScrollView style={SignUpScreenStyle.container}>
          <View style={SignUpScreenStyle.container}>
            <View
              style={{
                position: "relative",
                flex: 1,
                width: "100%",
                marginTop: "30%"
              }}
            >
              <View style={{ position: "relative", width: "100%" }}>
                <View style={SignUpScreenStyle.card}>
                  <TextField
                    ref={ref => {
                      this.fNameRef = ref;
                    }}
                    value={this.state.fName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ fName: text });
                    }}
                    onSubmitEditing={() => {
                      this.lNameRef.focus();
                    }}
                    returnKeyType="next"
                    label="First Name"
                    error={this.state.errFName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={16}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    containerStyle={{
                      marginStart: 10,
                      marginEnd: 10,
                      paddingTop: "28%"
                    }}
                    onBlur={() => this.setState({ errFName: "" })}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.lNameRef = ref;
                    }}
                    value={this.state.lName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ lName: text });
                    }}
                    onSubmitEditing={() => {
                      this.phoneRef.focus();
                    }}
                    returnKeyType="next"
                    label="Last Name"
                    error={this.state.errLName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={16}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    onBlur={() => this.setState({ errLName: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginStart: 10,
                      marginEnd: 10,
                      flex: 1
                    }}
                  >
                    <Dropdown
                      data={this.state.countryCodes}
                      containerStyle={{ width: 80 }}
                      value={this.state.countryCode}
                      onChangeText={(value, index, data) =>
                        this.setState({ countryCode: value })
                      }
                    />
                    <TextField
                      ref={ref => {
                        this.phoneRef = ref;
                      }}
                      value={this.state.phoneNumber}
                      autoCorrect={false}
                      enablesReturnKeyAutomatically={true}
                      onChangeText={text => this.onTextChange(text)}
                      onSubmitEditing={() => {
                        this.emailRef.focus();
                      }}
                      returnKeyType="next"
                      label="Phone Number"
                      keyboardType="number-pad"
                      textContentType="telephoneNumber"
                      dataDetectorTypes="phoneNumber"
                      error={this.state.errPhone}
                      maxLength={16}
                      placeholderTextColor="#222222"
                      textColor="#222222"
                      underlineColorAndroid={Colors.transparent}
                      tintColor="#222222"
                      fontSize={16}
                      inputContainerStyle={{
                        borderBottomColor: "#DFDFDF",
                        borderBottomWidth: 1
                      }}
                      onBlur={() => this.setState({ errPhone: "" })}
                      containerStyle={{
                        marginStart: 10,
                        borderBottomColor: "#DFDFDF",
                        borderBottomWidth: 1
                      }}
                      labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                      titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                      affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    />
                  </View>
                  <TextField
                    ref={ref => {
                      this.emailRef = ref;
                    }}
                    value={this.state.email}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ email: text });
                    }}
                    onSubmitEditing={() => {
                      this.cityRef.focus();
                    }}
                    editable={false}
                    returnKeyType="next"
                    label="Email"
                    keyboardType="email-address"
                    error={this.state.errEmail}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={16}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errEmail: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.cityRef = ref;
                    }}
                    value={this.state.city}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ city: text });
                    }}
                    onSubmitEditing={() => {
                      this.stateRef.focus();
                    }}
                    returnKeyType="next"
                    label="City"
                    error={this.state.errCity}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={16}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    autoCapitalize="none"
                    onBlur={() => this.setState({ errCity: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                  <TextField
                    ref={ref => {
                      this.stateRef = ref;
                    }}
                    value={this.state.stateName}
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onChangeText={text => {
                      this.setState({ stateName: text });
                    }}
                    onSubmitEditing={() => {
                      this.passRef.focus();
                    }}
                    returnKeyType="next"
                    label="State"
                    error={this.state.errStateName}
                    placeholderTextColor="#222222"
                    textColor="#222222"
                    underlineColorAndroid={Colors.transparent}
                    tintColor="#222222"
                    fontSize={16}
                    inputContainerStyle={{
                      borderBottomColor: "#DFDFDF",
                      borderBottomWidth: 1
                    }}
                    onBlur={() => this.setState({ errStateName: "" })}
                    containerStyle={{ marginStart: 10, marginEnd: 10,marginBottom:20 }}
                    labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                    affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                  />
                </View>

                <Button
                  loading={this.state.loading}
                  title="UPDATE"
                  buttonStyle={{ backgroundColor: "#3444D6", borderRadius: 20 }}
                  containerStyle={{
                    margin: 20
                  }}
                  onPress={() => this.doRegistration()}
                  titleStyle={{ fontFamily: "AktivGrotesk-Bold",fontSize:16 }}
                />
              </View>
              <View style={{ position: "absolute", top: -65, width: "100%" }}>
                <Image
                  source={
                    this.state.profilePicture == ""
                      ? Icons.user
                      : { uri: this.state.profilePicture }
                  }
                  style={{
                    height: 150,
                    width: 150,
                    alignSelf: "center",
                    borderRadius: 75
                  }}
                  resizeMethod="auto"
                  resizeMode="contain"
                  PlaceholderContent={<ActivityIndicator />}
                />
                <TouchableOpacity
                  style={{ alignSelf: "center", marginTop: 10 }}
                  onPress={() => this.pickSingle(true)}
                >
                  <Text
                    style={{
                      color: "#6570DE",
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Edit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default EditProfileScreen;
