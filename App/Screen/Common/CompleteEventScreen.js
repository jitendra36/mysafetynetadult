import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Colors from "../../Resource/Color/index";
import {
  Text,
  Image,
  Button,
  Input
} from "react-native-elements";
import { TextField } from "react-native-material-textfield";

class CompleteEventScreen extends PureComponent {
 
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      eventTitle: "Untitled Event",
      errEventTitle: "",
      eventStartDate: "Event Start Date",
      eventEndDate: "Event End Date",
      eventStartTime: "Event Start Time",
      eventEndTime: "Event End Time",
      invitedUsers: [
        {
          name: "Smith Clarrt",
          image:
            "http://ap.church/wp-content/uploads/2018/08/Brian-Jones-Profile.jpg"
        }
      ]
    };
  }

  doCompleteEvent() {
    const { navigate } = this.props.navigation;
    navigate("Tabs");
  }


  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: 100, height: 100, borderRadius: 4 }}
        />

        <Text
          style={{
            marginTop: 5,
            color: "#666666",
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 13,
            textAlign: "center"
          }}
        >
          {item.name}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
        <ScrollView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%"
              }}
            >
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventTitle}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventTitle: text });
                }}
                editable={false}
                returnKeyType="next"
                label="EVENT TITLE"
                error={this.state.errEventTitle}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={25}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: Colors.transparent,
                  borderBottomWidth: 0
                }}
                onBlur={() => this.setState({ errEventTitle: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                WITH <Text style={{ color: "#3444D6" }}> SMITH</Text>
              </Text>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                EVENT TIME
              </Text>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventStartDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventStartTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  disabled={true}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventEndDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    {this.state.eventEndTime}
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 1,
                  backgroundColor: "#CCCCCC",
                  marginStart: 10,
                  marginEnd: 10,
                  marginTop: 20,
                  marginBottom: 20
                }}
              />
              <Button
                loading={this.state.loading}
                title="VIEW MORE"
                onPress={()=>this.props.navigation.goBack(null)}
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                titleStyle={{
                  fontFamily: "AktivGrotesk-Bold",
                  fontSize: 16
                }}
                containerStyle={{
                  width: 180,
                  alignSelf: "center",
                  marginBottom: 20
                }}
              />
            </View>
            <Text
              style={{
                color: Colors.black,
                fontFamily: "AktivGrotesk-Regular",
                fontSize: 13,
                marginStart: "5%",
                marginEnd: "5%"
              }}
            >
              Fill out the following questions and complete your event.
            </Text>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%"
              }}
            >
              <Text
                style={{
                  color: Colors.black,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  marginTop: 20,
                  margin: 10,
                  flexWrap: "wrap",
                  flex: 1
                }}
              >
                Fill out the following questions and complete your event.
              </Text>
              <View
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  borderRadius: 2,
                  borderColor: Colors.black,
                  borderWidth: 0.5,
                  height: 80
                }}
              >
                <Input
                  value={this.state.answerOne}
                  onChangeText={text => this.setState({ answerOne: text })}
                  inputStyle={{
                    fontFamily: "AktivGrotesk-Regular",
                    fontSize: 13,
                    padding: 0
                  }}
                  multiline={true}
                  underlineColorAndroid={Colors.transparent}
                  inputContainerStyle={{padding:0,borderBottomWidth:0}}
                />
              </View>
              <Text
                style={{
                  color: Colors.black,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  margin: 10,
                  flexWrap: "wrap",
                  flex: 1
                }}
              >
                Fill out the following questions and complete your event.
              </Text>
              <View
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  borderRadius: 2,
                  borderColor: Colors.black,
                  borderWidth: 0.5,
                  height: 80
                }}
              >
                <Input
                  value={this.state.answerOne}
                  onChangeText={text => this.setState({ answerOne: text })}
                  inputStyle={{
                    fontFamily: "AktivGrotesk-Regular",
                    fontSize: 13,
                    padding: 0
                  }}
                  multiline={true}
                  underlineColorAndroid={Colors.transparent}
                  inputContainerStyle={{padding:0,borderBottomWidth:0}}
                />
              </View>
              <Text
                style={{
                  color: Colors.black,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  margin: 10,
                  flexWrap: "wrap",
                  flex: 1
                }}
              >
                Fill out the following questions and complete your event.
              </Text>
              <View
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20,
                  borderRadius: 2,
                  borderColor: Colors.black,
                  borderWidth: 0.5,
                  height: 80
                }}
              >
                <Input
                  value={this.state.answerOne}
                  onChangeText={text => this.setState({ answerOne: text })}
                  inputStyle={{
                    fontFamily: "AktivGrotesk-Regular",
                    fontSize: 13,
                    padding: 0
                  }}
                  multiline={true}
                  underlineColorAndroid={Colors.transparent}
                  inputContainerStyle={{padding:0,borderBottomWidth:0}}
                />
              </View>
            </View>
            <View
              style={{
                margin: 15
              }}
            >
              <Button
                loading={this.state.loading}
                title="COMPLETE YOUR EVENT"
                onPress={()=>this.doCompleteEvent()}
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                titleStyle={{
                  fontFamily: "AktivGrotesk-Bold",
                  fontSize: 16
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default CompleteEventScreen;
