import React, { Component } from "react";
import { View } from "react-native";
import NotificationListComponent from "../../Component/NotificationListComponent";
import colors from "../../Resource/Color";

export default class NotificationList extends Component {
  static navigationOptions = {
    title: "Notification"
  };
  constructor(props) {
    super(props);
    this.state = {
      sectionDotColor: ["#E06871", "#D547C3", "#4181DF"],
      Notification: [
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        }
      ]
    };
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.bg_color }}>
        <NotificationListComponent
          Notification={this.state.Notification}
          sectionDotColor={this.state.sectionDotColor}
          refreshing={false}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}
