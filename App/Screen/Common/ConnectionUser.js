import React, { Component } from "react";
import {
  ScrollView,
  SafeAreaView,
  Image,
  View,
  StyleSheet,
  Text,
  ActivityIndicator
} from "react-native";
import colors from "../../Resource/Color";
import images from "../../Resource/index";
import ConnectionUserComponent from "../../Component/ConnectionUserComponent";
import font from "../../Resource/fontFamily";

class ConnectionUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      userName: "zala Jitendra",
      userBadge: "dksjdkjs4564",
      city: "surendranagar",
      state: "lakhrat",
      profilePicture: "",
      eventList: [
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        },
        {
          event_title: "Bussiness meeting with Sameera",
          location: "NORTH CAPITOL ST NW",
          time: "10:00 AM - 12:00 PM"
        }
      ]
    };
  }
  render() {
    return (
      <SafeAreaView style={customStyles.container}>
        <ScrollView>
          <View style={customStyles.topView}>
            <View style={customStyles.cart}>
              <View style={customStyles.userView}>
                <View style={customStyles.imageView}>
                  <Image
                    source={
                      this.state.profilePicture === ""
                        ? images.user
                        : { uri: this.state.profilePicture }
                    }
                    style={customStyles.UserImage}
                  />
                  <Image source={images.tick} style={customStyles.tickImage} />
                </View>
              </View>
              <View style={customStyles.detailView}>
                <Text style={customStyles.userName}>{this.state.userName}</Text>
                <Text style={customStyles.userBadge}>
                  {this.state.userBadge}
                </Text>
                <View style={customStyles.location}>
                  <Text style={[customStyles.address, { textAlign: "right" }]}>
                    {this.state.city}
                  </Text>
                  <View style={customStyles.line} />
                  <Text style={[customStyles.address, { textAlign: "left" }]}>
                    {this.state.state}
                  </Text>
                </View>
              </View>
            </View>
            <Text style={customStyles.SCHEDULED}>SCHEDULED EVENTS</Text>
            <View style={customStyles.cart}>
              <ConnectionUserComponent
                Notification={this.state.eventList}
                navigation={this.props.navigation}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const customStyles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.bg_color },
  topView: { marginTop: 90, marginBottom: 30 },
  userView: { flex: 1, alignItems: "center" },
  cart: {
    marginStart: 20,
    marginEnd: 20,
    backgroundColor: colors.white,
    borderRadius: 5,
    position: "relative"
  },
  imageView: {
    bottom: 65,
    zIndex: 100,
    marginStart: 55,
    flexDirection: "row",
    alignItems: "center",
    position: "relative"
  },
  detailView: { alignItems: "center", marginTop: -30, marginBottom: 15 },
  UserImage: {
    height: 150,
    width: 150
  },
  tickImage: {
    top: 35,
    right: 35,
    zIndex: 100,
    height: 45,
    width: 45
  },

  location: { flexDirection: "row", width: "100%", marginTop: 5 },
  line: {
    width: 1,
    backgroundColor: colors.textColorDark,
    marginEnd: 5,
    marginStart: 5
  },
  SCHEDULED: { margin: 20 },
  userName: {
    fontSize: 18,
    fontFamily: font.Regular,
    color: colors.black
  },
  userBadge: {
    marginTop: 8,
    fontSize: 16,
    fontFamily: font.Regular,
    color: colors.textColorDark
  },
  address: {
    flex: 1,
    fontSize: 14,
    fontFamily: font.Regular,
    color: colors.textColorDark
  }
});
export default ConnectionUser;
