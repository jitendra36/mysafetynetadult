import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList
} from "react-native";
import Colors from "../../Resource/Color/index";
import { Text, Image, Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import moment from "moment";

class EventDetailScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      eventTitle: "Untitled Event",
      errEventTitle: "",
      eventStartDate: moment(new Date()).format("ddd D MMM, YYYY"),
      invitedUsers: [
        {
          name: "Smith Clarrt",
          image:
            "http://ap.church/wp-content/uploads/2018/08/Brian-Jones-Profile.jpg"
        }
      ]
    };
  }

  doCompleteEvent() {
    const { navigate } = this.props.navigation;
    navigate("CompleteEvent");
  }

  renderInvite(item, index) {
    return (
      <View
        style={{
          marginStart: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: item.image }}
          style={{ width: 100, height: 100, borderRadius: 4 }}
        />

        <Text
          style={{
            marginTop: 5,
            color: "#666666",
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 13,
            textAlign: "center"
          }}
        >
          {item.name}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
        <ScrollView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
            <View
              style={{
                backgroundColor: Colors.white,
                borderRadius: 4,
                elevation: 2,
                margin: "5%"
              }}
            >
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventTitle}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventTitle: text });
                }}
                editable={false}
                returnKeyType="next"
                label="EVENT TITLE"
                error={this.state.errEventTitle}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={25}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: Colors.transparent,
                  borderBottomWidth: 0
                }}
                onBlur={() => this.setState({ errEventTitle: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginBottom: 10
                }}
              >
                EVENT TIME
              </Text>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    {this.state.eventStartDate}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    4:40 PM
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 10 }}>
                <TouchableOpacity
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                  disabled={true}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black
                    }}
                  >
                    Tue 6 Feb, 2019
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={true}
                  style={{ marginStart: 10, marginEnd: 10, flex: 1 }}
                >
                  <Text
                    style={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: Colors.black,
                      textAlign: "right"
                    }}
                  >
                    4:40 PM
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30
                }}
              >
                LOCATION
              </Text>
              <TextField
                ref={ref => {
                  this.eventTitleRef = ref;
                }}
                value={this.state.eventLocation}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventLocation: text });
                }}
                returnKeyType="next"
                label="EVENT LOCATION"
                error={this.state.errEventLocation}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={13}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventLocation: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30,
                  marginBottom: 10
                }}
              >
                INVITES
              </Text>
              <View>
                <FlatList
                  horizontal={true}
                  data={this.state.invitedUsers}
                  renderItem={({ item, index }) =>
                    this.renderInvite(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                  refreshing={this.state.refreshing}
                  style={{ marginStart: 10, marginEnd: 10, marginBottom: 20 }}
                />
              </View>
              <Text
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: "AktivGrotesk-Regular",
                  fontSize: 13,
                  color: Colors.black,
                  marginTop: 30
                }}
              >
                NOTES
              </Text>
              <TextField
                ref={ref => {
                  this.eventNotesRef = ref;
                }}
                value={this.state.eventNotes}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={text => {
                  this.setState({ eventNotes: text });
                }}
                returnKeyType="next"
                label="NOTES"
                error={this.state.errEventNotes}
                placeholderTextColor="#222222"
                textColor="#222222"
                underlineColorAndroid={Colors.transparent}
                tintColor="#222222"
                fontSize={13}
                containerStyle={{
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20
                }}
                inputContainerStyle={{
                  borderBottomColor: "#DFDFDF",
                  borderBottomWidth: 1
                }}
                onBlur={() => this.setState({ errEventNotes: "" })}
                labelTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
                affixTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              />
            </View>
            <View
              style={{
                margin: 15
              }}
            >
              <Button
                loading={this.state.loading}
                onPress={() => this.doCompleteEvent()}
                title="COMPLETE YOUR EVENT"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20,
                  alignItems: "center"
                }}
                titleStyle={{
                  fontFamily: "AktivGrotesk-Bold",
                  fontSize: 16
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default EventDetailScreen;
