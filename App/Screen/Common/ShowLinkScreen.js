import React, { PureComponent } from "react";
import { View, SafeAreaView, TouchableOpacity } from "react-native";

import { WebView } from "react-native-webview";

class ShowLinkScreen extends PureComponent {
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
        <View style={{ flex: 1, backgroundColor: "#DFE2F1" }}>
          <WebView
            scalesPageToFit={false}
            source={{ uri:"www.google.co.in" }}
            style={{ margin: 5, backgroundColor: "transparent",flex: 1 }}
            bounces={false}
            javaScriptEnabled={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}
export default ShowLinkScreen;
