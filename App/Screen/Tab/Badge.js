import React, { PureComponent } from "react";
import { View, SafeAreaView } from "react-native";
import { Text, Button } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import BadgeScreenStyle from "../../Resource/Style/BadgeScreenStyle";
import { TextField } from "react-native-material-textfield";
import Header from "../../Component/Header";

class Badge extends PureComponent {
 
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isError: false,
      badgeNumber: "",
      errBadgeNumber: ""
    };
  }
  render() {
    return (
      <SafeAreaView style={BadgeScreenStyle.container}>
       <Header navigation={this.props.navigation} name={"BADGE"} notify={true}/> 
        <View style={BadgeScreenStyle.container}>
          <View style={BadgeScreenStyle.card}>
            <TextField
              ref={ref => {
                this.badgeNumberRef = ref;
              }}
              value={this.state.badgeNumber}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={text => {
                this.setState({ badgeNumber: text });
              }}
              returnKeyType="done"
              label="BADGE NUMBER"
              error={this.state.errBadgeNumber}
              placeholderTextColor="#222222"
              textColor="#222222"
              underlineColorAndroid={Colors.transparent}
              tintColor="#222222"
              fontSize={25}
              containerStyle={{
                marginStart: 10,
                marginEnd: 10,
                display: this.state.isError ? "none" : "flex"
              }}
              
              inputContainerStyle={{
                borderBottomColor: "#C3C3C3",
                borderBottomWidth: 1
              }}
              onBlur={() => this.setState({ errBadgeNumber: "" })}
              labelTextStyle={{ fontFamily: "AktivGrotesk-Regular"  }}
              titleTextStyle={{ fontFamily: "AktivGrotesk-Regular" }}
              affixTextStyle={{ fontFamily: "AktivGrotesk-Regular"}}
            />
            <View style={{ display: this.state.isError ? "flex" : "none" }}>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 30,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#E82D2E",
                  marginBottom: 20
                }}
              >
                ERROR
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#222222",
                  marginBottom: 20
                }}
              >
                Badge is not verified..
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "AktivGrotesk-Regular",
                  color: "#222222",
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 20
                }}
              >
                Please enter valid Badge number or ask to repeat badge number
              </Text>
            </View>
          </View>
          <View style={BadgeScreenStyle.view_button}>
            <Button
              loading={this.state.loading}
              title={this.state.isError ? "VERIFY AGAIN" : "VERIFY"}
              buttonStyle={{
                backgroundColor: "#3444D6",
                borderRadius: 20
              }}
              titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
export default Badge;
