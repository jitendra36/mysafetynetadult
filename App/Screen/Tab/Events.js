import React, { PureComponent } from "react";
import {
  View,
  SafeAreaView,
  SectionList,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Text, Image } from "react-native-elements";

import EventScreenStyle from "../../Resource/Style/EventScreenStyle";
import { Calendar } from "react-native-calendars";
import moment from "moment";
import Header from "../../Component/Header";
import Icons from "../../Resource/index";
import Colors from "../../Resource/Color/index";
import EventListComponent from "../../Component/EventListComponent";

class Events extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      upcomingEvents: [
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        },
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        },
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        }
      ],
      sectionDotColor: ["#E06871", "#D547C3", "#4181DF"],
      selectedDay: moment(new Date()).format("YYYY-MM-DD"),
      previousMonth: "",
      nextMonth: ""
    };
  }
  componentWillMount() {
    var datePrevious = new Date();
    datePrevious.setMonth(datePrevious.getMonth() - 1);
    var dateNext = new Date();
    dateNext.setMonth(dateNext.getMonth() + 1);
    this.setState({ previousMonth: moment(datePrevious).format("MMM YYYY") });
    this.setState({ nextMonth: moment(dateNext).format("MMM YYYY") });
  }
  renderItemSeprator() {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#CCCCCC",
          marginStart: 10,
          marginEnd: 10
        }}
      />
    );
  }
  renderSectionHeader(section) {
    return (
      <View
        style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}
      >
        <View
          style={{
            flex: 1,
            height: 1,
            backgroundColor: "#BFBFBF",
            marginStart: 10,
            marginEnd: 5
          }}
        />
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 15,
            color: "#2640DF"
          }}
        >
          {section.title}
        </Text>
        <View
          style={{
            flex: 1,
            height: 1,
            backgroundColor: "#BFBFBF",
            marginStart: 5,
            marginEnd: 10
          }}
        />
      </View>
    );
  }
  renderSectionItem(item, index, section) {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          marginTop: 10,
          marginBottom: 10,
          marginStart: 5,
          marginEnd: 10
        }}
      >
        <View
          style={{
            width: 15,
            height: 15,
            backgroundColor: this.state.sectionDotColor[
              Math.floor(Math.random() * this.state.sectionDotColor.length)
            ],
            borderRadius: 7.5,
            margin: 5
          }}
        />
        <View style={{ flex: 1 }}>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 13,
              color: Colors.black
            }}
          >
            {item.event_title}
          </Text>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#888888"
            }}
          >
            {item.location}
          </Text>
        </View>

        <View>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#666666"
            }}
          >
            {item.time}
          </Text>
        </View>
      </View>
    );
  }
  renderConnectionItem(item, index) {
    return (
      <View
        style={{
          backgroundColor: Colors.white,
          flexDirection: "row",
          borderRadius: 4,
          marginEnd: 10,
          alignItems: "center",
          height: 80
        }}
      >
        <View style={{ flexDirection: "row", margin: 5, marginStart: 10 }}>
          <Image
            source={{ uri: item.image }}
            style={{ width: 60, height: 60, borderRadius: 30 }}
          />
          <Image
            source={Icons.tick}
            style={{
              height: 15,
              width: 15,
              position: "absolute",

              top: 0,
              end: -4
            }}
          />
        </View>
        <View style={{ flex: 1, marginStart: 10, marginEnd: 10 }}>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 14,
              color: Colors.black
            }}
          >
            {item.name}
          </Text>
          <Text
            style={{
              fontFamily: "AktivGrotesk-Regular",
              fontSize: 12,
              color: "#666666"
            }}
          >
            Badge Number : {item.badgeNumber}
          </Text>
        </View>
      </View>
    );
  }
  _renderArrow = direction => {
    if (direction === "left") {
      return (
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 12,
            color: "#ADADAD"
          }}
        >
          {this.state.previousMonth}
        </Text>
      );
    } else {
      return (
        <Text
          style={{
            fontFamily: "AktivGrotesk-Regular",
            fontSize: 12,
            color: "#ADADAD"
          }}
        >
          {this.state.nextMonth}
        </Text>
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={EventScreenStyle.container}>
        <Header
          navigation={this.props.navigation}
          name={"EVENTS"}
          notify={true}
        />
        <ScrollView style={{marginBottom:5,marginTop:5}}>
          <View style={EventScreenStyle.container}>
            <Calendar
              // Initially visible month. Default = Date()
              current={new Date()}
              markedDates={{ [this.state.selectedDay]: { selected: true } }}
              // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
              // Handler which gets executed on day press. Default = undefined
              onDayPress={day => {
                console.log("selected day", day);
                this.setState({ selectedDay: day.dateString });
              }}
              // Handler which gets executed on day long press. Default = undefined
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={"MMM yyyy"}
              // Handler which gets executed when visible month changes in calendar. Default = undefined
              onMonthChange={month => {
                console.log("month changed", month.dateString);
                var datePrevious = new Date(month.dateString);
                datePrevious.setMonth(datePrevious.getMonth() - 1);
                var dateNext = new Date(month.dateString);
                dateNext.setMonth(dateNext.getMonth() + 1);
                console.log(
                  "Previous ",
                  moment(datePrevious).format("MMM YYYY"),
                  "Next ",
                  moment(dateNext).format("MMM YYYY")
                );

                this.setState({
                  previousMonth: moment(datePrevious).format("MMM YYYY")
                });
                this.setState({
                  nextMonth: moment(dateNext).format("MMM YYYY")
                });
              }}
              // Hide month navigation arrows. Default = false
              hideArrows={false}
              // Replace default arrows with custom ones (direction can be 'left' or 'right')

              // Do not show days of other months in month page. Default = false
              hideExtraDays={false}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={true}
              // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
              firstDay={0}
              // Hide day names. Default = false
              hideDayNames={false}
              // Show week numbers to the left. Default = false
              showWeekNumbers={false}
              // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              onPressArrowLeft={substractMonth => substractMonth()}
              // Handler which gets executed when press arrow icon left. It receive a callback can go next month
              onPressArrowRight={addMonth => addMonth()}
              renderArrow={this._renderArrow}
              theme={{
                textDisabledColor: "#D1D1D1",
                dayTextColor: "#222222",
                textDayFontFamily: "AktivGrotesk-Regular",
                textMonthFontFamily: "AktivGrotesk-Regular",
                textDayHeaderFontFamily: "AktivGrotesk-Regular",
                selectedDayBackgroundColor: "#2640DF",
                monthTextColor: "#2640DF",
                todayTextColor: "#2640DF",
                selectedDayTextColor: "#FFFFFF",
                textDayFontSize: 12,
                textDayHeaderFontSize: 12,
                textMonthFontSize: 12
              }}
            />
            <View style={{ marginTop: 10 }}>
              <EventListComponent
                navigation={this.props.navigation}
                upcomingEvents={this.state.upcomingEvents}
                sectionDotColor={this.state.sectionDotColor}
              />
            </View>

            {/* <SectionList
              renderItem={({ item, index, section }) =>
                this.renderSectionItem(item, index, section)
              }
              renderSectionHeader={({ section }) =>
                this.renderSectionHeader(section)
              }
              ItemSeparatorComponent={() => this.renderItemSeprator()}
              sections={this.state.upcomingEvents}
              keyExtractor={(item, index) => item + index}
              stickySectionHeadersEnabled={false}
              style={{
                margin: "5%",
                backgroundColor: Colors.white,
                borderRadius: 4
              }}
            /> */}
          </View>
        </ScrollView>
        <TouchableOpacity>
          <Image
            source={Icons.plus_icon}
            style={{
              position: "absolute",
              bottom: 0,
              end: 0,
              width: 50,
              height: 50,
              marginBottom: 15,
              marginEnd: 15
            }}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
export default Events;
