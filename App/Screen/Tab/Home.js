import React, { PureComponent } from "react";
import { View, SafeAreaView, TouchableOpacity, StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import Header from "../../Component/Header";
import ConnectionListComponent from "../../Component/ConnectionListComponent";
import EventListComponent from "../../Component/EventListComponent";

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      myConnections: [
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        },
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        },
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        },
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        },
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        },
        {
          image:
            "https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754",
          name: "Ethan Clark",
          badgeNumber: "#6452421"
        }
      ],
      upcomingEvents: [
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        },
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        },
        {
          title: "Today 5",
          data: [
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            },
            {
              event_title: "Bussiness meeting with Sameera",
              location: "NORTH CAPITOL ST NW",
              time: "10:00 AM - 12:00 PM"
            }
          ]
        }
      ],
      sectionDotColor: ["#E06871", "#D547C3", "#4181DF"]
    };
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header
          navigation={this.props.navigation}
          name={"HOME"}
          notify={true}
        />
        <View style={styles.container}>
          <View style={styles.connectionView}>
            <Text style={styles.textView}>MY CONNECTIONS</Text>
            <TouchableOpacity>
              <Text style={styles.viewAll}>VIEW ALL</Text>
            </TouchableOpacity>
          </View>

          <ConnectionListComponent
            navigation={this.props.navigation}
            Connections={this.state.myConnections}
            refreshing={this.state.refreshing}
          />

          <View style={styles.connectionView}>
            <Text style={styles.textView}>UPCOMING EVENTS</Text>
            <TouchableOpacity>
              <Text style={styles.viewAll}>VIEW ALL</Text>
            </TouchableOpacity>
          </View>
          <EventListComponent
            navigation={this.props.navigation}
            upcomingEvents={this.state.upcomingEvents}
            sectionDotColor={this.state.sectionDotColor}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#DFE2F1" },
  connectionView: {
    flexDirection: "row",
    marginStart: "5%",
    marginEnd: "5%",
    marginTop: 20,
    marginBottom: 10
  },
  textView: {
    flex: 1,
    fontFamily: "AktivGrotesk-Regular",
    fontSize: 14,
    color: Colors.black
  },
  viewAll: {
    flex: 1,
    fontFamily: "AktivGrotesk-Regular",
    fontSize: 14,
    textAlign: "right",
    color: "#2640DF"
  }
});
export default Home;
