import React, { PureComponent } from "react";
import { View, SafeAreaView, TouchableOpacity, ScrollView } from "react-native";
import {  Text, Button, Image, Icon } from "react-native-elements";
import Colors from "../../Resource/Color/index";
import Icons from "../../Resource/index";
import SettingScreenStyle from "../../Resource/Style/SettingScreenStyle";
import Header from "../../Component/Header";

class SettingScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      userName: "User Name",
      email: "username@gmail.com",
      errBadgeNumber: ""
    };
  }
  doRedirect(screen) {
    this.props.navigation.navigate(screen);
  }
  doRedirectLink(title) {
    this.props.navigation.navigate("ShowLink", { title: title });
  }
  render() {
    return (
      <SafeAreaView style={SettingScreenStyle.container}>
        <Header
          navigation={this.props.navigation}
          name={"SETTING"}
          notify={true}
        />
        <ScrollView style={SettingScreenStyle.container}>
          <View style={SettingScreenStyle.container}>
            <View style={SettingScreenStyle.card}>
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: "#3344D6",
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4
                }}
              >
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={Icons.user}
                    style={{
                      width: 100,
                      height: 100,
                      margin: "5%",
                      marginTop: 10,
                      marginBottom: 10,
                      borderRadius: 50
                    }}
                  />
                </View>
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.state.userName}
                  </Text>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    {this.state.email}
                  </Text>
                  <Button
                    title="Edit Profile"
                    buttonStyle={{
                      backgroundColor: Colors.white,
                      borderRadius: 20
                    }}
                    containerStyle={{ width: 130, marginTop: 10 }}
                    titleStyle={{
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 13,
                      color: "#3444D6"
                    }}
                    onPress={() => this.doRedirect("EditProfile")}
                  />
                </View>
              </View>
              <TouchableOpacity onPress={() => this.doRedirect("Subscription")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Subscription
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirect("ManageCard")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Manage Cards
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.doRedirect("ChangePassword")}
              >
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Change Password
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Partners")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Partners
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Help and Feedback")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Help and Feedback
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("About Us")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    About Us
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Contact Us")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Contact Us
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("FAQs")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    FAQs
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.doRedirectLink("Privacy Policy")}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#CCCCCC",
                    flexDirection: "row",
                    marginStart: 10,
                    marginEnd: 10,
                    marginBottom: 20,
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      marginTop: 15,
                      marginBottom: 15,
                      flex: 1,
                      color: Colors.black,
                      fontFamily: "AktivGrotesk-Regular",
                      fontSize: 16
                    }}
                  >
                    Privacy Policy
                  </Text>
                  <Icon name="keyboard-arrow-right" size={30} color="#3444D6" />
                </View>
              </TouchableOpacity>
            </View>
            <View style={SettingScreenStyle.view_button}>
              <Button
                loading={this.state.loading}
                title="LOG OUT"
                buttonStyle={{
                  backgroundColor: "#3444D6",
                  borderRadius: 20
                }}
                titleStyle={{ fontFamily: "AktivGrotesk-Bold", fontSize: 16 }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
export default SettingScreen;
