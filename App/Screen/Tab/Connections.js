import React, { Component } from "react";
import { Text, View,SafeAreaView ,Image } from "react-native";
import Header from "../../Component/Header";


export default class Connections extends Component {
 
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
       <Header navigation={this.props.navigation} name={"CONNECTIONS"} notify={true}/>
        <Text>Connections</Text>
      </SafeAreaView>
    );
  }
}
