import React from "react";
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
  createAppContainer
} from "react-navigation";

//tab screen
import Home from "./Screen/Tab/Home";
import Connections from "./Screen/Tab/Connections";
import Events from "./Screen/Tab/Events";
import Badge from "./Screen/Tab/Badge";
import Setting from "./Screen/Tab/Setting";
import TabBarCustom from "./Component/TabBarCustom";

//login module
import Login from "./Screen/login/LoginScreen";
import SignUp from "./Screen/login/RegistrationScreen";

//common module
import Notification from "./Screen/Common/NotificationList";
import NotificationDetail from "./Screen/Common/NotificationDetail";
import ConnectionUser from "./Screen/Common/ConnectionUser";
import EventDetail from "./Screen/Common/EventDetailScreen";
import CompleteEvent from "./Screen/Common/CompleteEventScreen";
import ShowLink from "./Screen/Common/ShowLinkScreen";

const Tabs = createBottomTabNavigator(
  {
    Home: Home,
    Connections: Connections,
    Events: Events,
    Badge: Badge,
    Setting: Setting
  },
  {
    navigationOptions: { header: null },
    tabBarComponent: props => <TabBarCustom {...props} />,
    swipeEnabled: false,
    animationEnabled: true
  }
);

const StackNavigation = createStackNavigator(
  {
    Login: Login,
    SignUp: SignUp,
    ConnectionUser: ConnectionUser,
    Tabs: Tabs,
    Notification: Notification,
    NotificationDetail: NotificationDetail,
    EventDetail: EventDetail,
    CompleteEvent: CompleteEvent,
    ShowLink: ShowLink
  },
  {
    initialRouteName: "Login"
  }
);

// const createRootNavigator = (signedIn = false) => {
//   return createSwitchNavigator(
//     {
//       SignedIn: {
//         screen: Login
//       },
//       SignedOut: {
//         screen: Tabs
//       }
//     },
//     {
//       initialRouteName: signedIn ? "SignedIn" : "SignedOut"
//     }
//   );
// };

const App = createAppContainer(StackNavigation);
export default App;
