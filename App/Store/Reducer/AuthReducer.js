import * as actionType from '../ActionType'

const initialState={
    User:[],
}

const Auth=(state = initialState, action)=>{
    switch (action.type) {
        case actionType.GET_LOGIN_SUCCESS:
        return {
            ...state, 
            User: action.User,
        };
        default:
            return state; 
    }
}
export default Auth;