import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  View,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color/index";
import font from "../Resource/fontFamily";

class ConnectionUserComponent extends Component {
  doClick(screen, Data) {
    // const { navigate } = this.props.navigation;
    // navigate(screen, { NotificationDetail: Data });
  }

  _renderItem(item, index) {
    console.log("Connection list", item);
    return (
      <View>
        <TouchableOpacity
          style={style.notificationView}
          onPress={() => this.doClick("NotificationDetail", item)}
        >
          <View>
            <Text style={style.notificationText}>{item.event_title}</Text>
            <Text style={style.notificationLocationText}>{item.location}</Text>
            <Text style={style.notificationTimeText}>{item.time}</Text>
          </View>
        </TouchableOpacity>
        <View style={style.underline} />
      </View>
    );
  }

  render() {
    return (
      <View >
        <ActivityIndicator
          size="large"
          style={{
            display: this.props.refreshing ? "flex" : "none",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        />
        <FlatList
          data={this.props.Notification}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
ConnectionUserComponent.propTypes = {
  navigation: PropTypes.object,
  Notification: PropTypes.array
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 5,
    marginTop: 20,
   
  },
  underline: {
    width: "90%",
    height: 1,
    backgroundColor: colors.bg_color,
    alignSelf: "center",
    marginStart: 0,
    marginTop: 0,
    marginBottom: 0
  },
  notificationView: {
    marginTop: 15,
    marginBottom: 15,
    marginStart: 15,
    marginEnd: 15
  },

  notificationText: {
    fontFamily: font.Regular,
    fontSize: 13,
    color: colors.black
  },
  notificationLocationText: {
    marginTop: 5,
    fontFamily: font.Regular,
    fontSize: 12,
    color: colors.textColorLight
  },
  notificationTimeText: {
    fontFamily: font.Regular,
    fontSize: 12,
    color: colors.textColorDark
  }
});

export default ConnectionUserComponent;
