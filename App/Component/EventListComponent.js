import React, { PureComponent } from "react";
import { View, TouchableOpacity, StyleSheet, SectionList } from "react-native";
import { Text } from "react-native-elements";
import Colors from "../Resource/Color/index";
import PropTypes from "prop-types";
import font from "../Resource/fontFamily";

class EventListComponent extends PureComponent {
  doClick(Data) {
    const { navigate } = this.props.navigation;
    navigate("EventDetail", { EventDetail: Data });
  }

  renderItemSeparator() {
    return <View style={styles.ItemSeparator} />;
  }
  renderSectionHeader(section) {
    return (
      <View style={styles.sectionContainer}>
        <View style={styles.sectionLeft} />
        <Text style={styles.sectionText}>{section.title}</Text>
        <View style={styles.sectionRight} />
      </View>
    );
  }
  renderSectionItem(item, index, section) {
    return (
      <TouchableOpacity style={styles.SectionItemContainer} onPress={()=>this.doClick(item)}>
        <View
          style={[
            styles.dotView,
            {
              backgroundColor: this.props.sectionDotColor[
                Math.floor(Math.random() * this.props.sectionDotColor.length)
              ]
            }
          ]}
        />
        <View style={styles.flex}>
          <Text style={styles.eventTitle}>{item.event_title}</Text>
          <Text style={styles.location}>{item.location}</Text>
        </View>
        <View>
          <Text style={styles.time}>{item.time}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          renderItem={({ item, index, section }) =>
            this.renderSectionItem(item, index, section)
          }
          renderSectionHeader={({ section }) =>
            this.renderSectionHeader(section)
          }
          ItemSeparatorComponent={() => this.renderItemSeparator()}
          sections={this.props.upcomingEvents}
          keyExtractor={(item, index) => item + index}
          stickySectionHeadersEnabled={false}
          style={styles.listView}
        />
      </View>
    );
  }
}

EventListComponent.propTypes = {
  navigation: PropTypes.object,
  upcomingEvents: PropTypes.array,
  sectionDotColor: PropTypes.array
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.bg_color },
  listView: {
    marginStart: "5%",
    marginEnd: "5%",
    marginBottom: "5%",
    backgroundColor: Colors.white,
    borderRadius: 4
  },
  ItemSeparator: {
    height: 1,
    backgroundColor: Colors.itemSeparator,
    marginStart: 10,
    marginEnd: 10
  },
  sectionContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10
  },
  sectionLeft: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.line,
    marginStart: 10,
    marginEnd: 5
  },
  sectionText: {
    fontFamily: font.Regular,
    fontSize: 15,
    color: Colors.blue
  },
  sectionRight: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.line,
    marginStart: 5,
    marginEnd: 10
  },
  SectionItemContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 15,
    marginBottom: 15,
    marginStart: 5,
    marginEnd: 10
  },
  dotView: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    margin: 5
  },
  flex: { flex: 1 },
  eventTitle: {
    fontFamily: font.Regular,
    fontSize: 13,
    color: Colors.black
  },
  location: {
    marginTop: 5,
    fontFamily: font.Regular,
    fontSize: 12,
    color: Colors.textColorLight
  },
  time: {
    fontFamily: font.Regular,
    fontSize: 12,
    color: Colors.textColorDark
  }
});

export default EventListComponent;
