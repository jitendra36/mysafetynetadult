import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  View,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color";
import font from "../Resource/fontFamily";

class NotificationListComponent extends Component {
  goNotification(screen, Data) {
    const { navigate } = this.props.navigation;
    navigate(screen, { NotificationDetail: Data });
  }

  _renderItem(item, index) {
    console.log("notification list", item);
    return (
      <View>
        <TouchableOpacity
          style={style.notificationView}
          onPress={() => this.goNotification("NotificationDetail", item)}
        >
          <View
            style={[
              style.dotView,
              {
                backgroundColor: this.props.sectionDotColor[
                  Math.floor(Math.random() * this.props.sectionDotColor.length)
                ]
              }
            ]}
          />
          <View style={{ flex: 1 }}>
            <Text style={style.notificationText}>{item.event_title}</Text>
            <Text style={style.notificationLocationText}>{item.location}</Text>
          </View>

          <View>
            <Text style={style.notificationTimeText}>{item.time}</Text>
          </View>
        </TouchableOpacity>
        <View style={style.underline} />
      </View>
    );
  }
  renderOnRefresh() {
    this.setprops({
      refreshing: true
    });
  }
  render() {
    return (
      <View elevation={5} style={style.container}>
        <ActivityIndicator
          size="large"
          style={{
            display: this.props.refreshing ? "flex" : "none",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        />
        <FlatList
          onRefresh={() => this.renderOnRefresh()}
          data={this.props.Notification}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.props.refreshing}
          style={{
            display: this.props.refreshing ? "none" : "flex"
          }}
        />
      </View>
    );
  }
}
NotificationListComponent.propTypes = {
  navigation: PropTypes.object,
  Notification: PropTypes.array,
  refreshing: PropTypes.bool,
  sectionDotColor: PropTypes.array
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 5,
    margin:"5%"
  },
  underline: {
    width: "90%",
    height: 0.8,
    backgroundColor: colors.bg_color,
    alignSelf: "center",
    marginStart: 15,
    marginTop: 10,
    marginBottom: 10
  },
  notificationView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    marginStart: 5,
    marginEnd: 10
  },
  dotView: { width: 15, height: 15, borderRadius: 7.5, margin: 5 },
  notificationText: {
    fontFamily: font.Regular,
    fontSize: 13,
    color: colors.black
  },
  notificationLocationText: {
    fontFamily: font.Regular,
    fontSize: 12,
    color: "#888888"
  },
  notificationTimeText: {
    fontFamily: font.Regular,
    fontSize: 12,
    color: "#666666"
  }
});

export default NotificationListComponent;
