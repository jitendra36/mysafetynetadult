import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import IconTab from "./IconTab";
import colors from "../Resource/Color/index";

class TabBarCustom extends Component {
  render() {
    const { navigation, jumpTo } = this.props;
    const { routes } = navigation.state;

    return (
      <View style={styles.tabContainer}>
        <View style={styles.subContainer}>
          {routes &&
            routes.map((route, index) => {
              const focused = index === navigation.state.index;
              return (
                <View key={route.key} style={styles.tabView}>
                  <IconTab
                    press={() => jumpTo(route.key)}
                    key={route.key}
                    index={index}
                    focused={focused}
                  />
                </View>
              );
            })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabContainer: { flexDirection: "row" },
  subContainer: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  tabView: {
    height: 60,
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:colors.blue
  }
});

export default TabBarCustom;
