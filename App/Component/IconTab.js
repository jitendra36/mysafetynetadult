import React, { Component } from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import Resource from "../Resource/index";
import colors from "../Resource/Color/index";
import string from "../Resource/string/index";

class IconTab extends Component {
  render() {
    let icon = Resource.tab_home;
    let Name = string.tabHome;
    const { press, focused, index } = this.props;

    if (index === 0) {
      icon = Resource.tab_home;
      Name = string.tabHome;
    } else if (index === 1) {
      icon = Resource.tab_connection;
      Name = string.tabConnections;
    } else if (index === 2) {
      icon = Resource.tab_event;
      Name = string.tabEvents;
    } else if (index === 3) {
      icon = Resource.tab_badge;
      Name = string.tabBadge;
    } else if (index === 4) {
      icon = Resource.tab_setting;
      Name = string.tabSetting;
    } else {
      icon = Resource.tab_setting;
      Name = string.tabHome;
    }
    return (
      <TouchableOpacity onPress={press}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: 60,
            width: 60,
            borderRadius: 30,
            backgroundColor: focused ? colors.bg_color : "transparent",
            bottom: focused ? 20 : 0
          }}
        >
          <View
            style={{
              height: 50,
              width: 50,
              borderRadius: 25,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: focused ? colors.blue : "transparent"
            }}
          >
            <Image
              style={{
                height: focused ? 28 : 22,
                width: focused ? 28 : 22,
                tintColor: focused ? colors.white : colors.image_tint
              }}
              source={icon}
            />
            <Text
              style={{
                fontSize: 8,
                display: focused ? "none" : "flex",
                color: colors.image_tint
              }}
            >
              {Name}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
export default IconTab;
