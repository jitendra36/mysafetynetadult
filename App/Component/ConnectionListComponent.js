import React, { PureComponent } from "react";
import { View, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import { Text, Image } from "react-native-elements";
import Colors from "../Resource/Color/index";
import Icons from "../Resource/index";
import PropTypes from "prop-types";
import font from "../Resource/fontFamily";

class ConnectionListComponent extends PureComponent {
  doClick(Data) {
    const { navigate } = this.props.navigation;
    navigate("ConnectionUser", { ConnectionUser: Data });
  }
  renderConnectionItem(item, index) {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.doClick(item)}
      >
        <View style={styles.imageView}>
          <Image source={{ uri: item.image }} style={styles.userImage} />
          <Image source={Icons.tick} style={styles.tickImage} />
        </View>
        <View style={styles.detailView}>
          <Text style={styles.nameView}>{item.name}</Text>
          <Text style={styles.badgeView}>
            Badge Number : {item.badgeNumber}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.props.Connections}
          renderItem={({ item, index }) =>
            this.renderConnectionItem(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
          refreshing={this.props.refreshing}
          style={styles.list}
        />
      </View>
    );
  }
}
ConnectionListComponent.propTypes = {
  navigation: PropTypes.object,
  Connections: PropTypes.array,
  refreshing: PropTypes.bool
};

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: Colors.white,
    flexDirection: "row",
    borderRadius: 4,
    marginEnd: 10,
    alignItems: "center",
    height: 80
  },
  imageView: { flexDirection: "row", margin: 5, marginStart: 10 },
  userImage: { width: 60, height: 60, borderRadius: 30 },
  tickImage: {
    height: 15,
    width: 15,
    position: "absolute",
    top: 0,
    end: -4
  },
  detailView: { flex: 1, marginStart: 10, marginEnd: 10 },
  nameView: {
    fontFamily: font.Regular,
    fontSize: 14,
    color: Colors.black
  },
  badgeView: {
    fontFamily: font.Regular,
    fontSize: 12,
    color: Colors.textColorDark
  },
  container: { backgroundColor: Colors.bg_color },
  list: { marginStart: "5%", marginEnd: "5%" }
});

export default ConnectionListComponent;
