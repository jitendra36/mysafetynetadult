import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import colors from "../Resource/Color";
import Resource from "../Resource/index";

class Header extends Component {
  goNotification(screen) {
    const { navigate } = this.props.navigation;
    navigate(screen);
  }
  render() {
    return (
      <View style={style.container} elevation={5}>
        <View style={style.containerSubView}>
          <Text style={style.text}>{this.props.name}</Text>
        </View>
        <TouchableOpacity
          style={style.containerSubView}
          onPress={() => {
            this.goNotification("Notification")
          }}
        >
          <Image source={Resource.alarm} style={style.image} />
          <View
            style={[
              style.dot,
              {
                backgroundColor: this.props.notify
                  ? colors.dotColor
                  : colors.transparent
              }
            ]}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
Header.propTypes = {
  navigation: PropTypes.object,
  name: PropTypes.any,
  notify: PropTypes.bool
};

const style = StyleSheet.create({
  container: {
    shadowColor: colors.bg_color,
    shadowOpacity: 1,
    shadowOffset: {height:1,width:0},
    flexDirection: "row",
    backgroundColor: colors.white,
    padding: 15,
    justifyContent: "space-between"
  },
  containerSubView: { flexDirection: "row", alignItems: "center" },
  text: { color: colors.black, fontSize: 16, fontFamily: "AktivGrotesk-Bold" },
  image: { width: 24, height: 24, overflow: "hidden" },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    position: "absolute",
    top: 0
  }
});

export default Header;
