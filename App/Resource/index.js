const index = {
  tab_home: require("../../assets/Icon/home.png"),
  tab_connection: require("../../assets/Icon/connections.png"),
  tab_event: require("../../assets/Icon/event.png"),
  tab_badge: require("../../assets/Icon/badge.png"),
  tab_setting: require("../../assets/Icon/setting.png"),

  user: require("../../assets/Image/user.png"),
  bg_login: require("../../assets/Image/bg_login.png"),
  bg_cart: require("../../assets/Image/bg_cart.png"),
  badge_white:require('../../assets/Icon/badge_white.png'),
  checked:require('../../assets/Icon/checked.png'),
  alarm:require('../../assets/Icon/alarm_blue.png'),
  tick:require('../../assets/Icon/Tick.png'),
 

 
};
export default index;
