const colors = {
    blue:"#0330BF",
    white:"#FFFFFF",
    black:"#000000",
    image_tint:"#959EC9",
    bg_color : "#DFE2F1",
    transparent:'transparent',
    inputTextColor:"#222222",
    forgotText:"#3B50E0",
    TextField_bg:"#DFDFDF",
    loginButton:"#3444D6",
    signUpButton:'#FF7404',
    dotColor:'#FF7305',
    textColorLight: "#888888",
    textColorDark: "#666666",
    itemSeparator:'#CCCCCC',
    line:'#BFBFBF',
  };
  export default colors;