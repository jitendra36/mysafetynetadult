import { StyleSheet} from 'react-native';
import Colors from "../Color/index";
const PaymentVerifiedScreenStyle = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#DFE2F1',
    },
    card:{
      backgroundColor: Colors.white,
      borderRadius: 4,
      alignItems: "center",
      margin: "5%"
    },
    checked:{
      width: 100,
      height: 100,
      margin: "5%",
      marginTop: "10%"
    },
    text_success:{
      margin: "5%",
      marginBottom: "10%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 15,
      color: Colors.black
    },
    view_button:{
      width: "90%",
      position: "absolute",
      bottom: 0,
      marginBottom: "5%",
      marginStart: 20,
      marginEnd: 20
    }
  });

  export default PaymentVerifiedScreenStyle;