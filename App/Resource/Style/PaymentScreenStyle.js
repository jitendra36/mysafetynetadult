import { StyleSheet} from 'react-native';
import Colors from "../Color/index";
const PaymentScreenStyle = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#DFE2F1',
    },
    card:{
     
     
    },
    checked:{
      margin: "5%",
      flexWrap: "wrap",
      width:'90%',
      height: 150 ,
    },
    text_success:{
      margin: "5%",
      marginBottom: "10%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 15,
      color: Colors.black
    },
    view_button:{
      width: "90%",
      position: "absolute",
      bottom: 0,
      marginBottom: "5%",
      marginStart: 20,
      marginEnd: 20
    }
  });

  export default PaymentScreenStyle;