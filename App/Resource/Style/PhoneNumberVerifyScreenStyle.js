import { StyleSheet} from 'react-native';
import Colors from "../Color/index";
const PhoneNumberVerifyScreenStyle = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#DFE2F1',
    },
    card:{
      backgroundColor: Colors.white,
      borderRadius: 4,
      alignItems: "center",
      margin: "5%"
    },
    checked:{
      width: 100,
      height: 100,
      margin: "5%",
      marginTop: "10%"
    },
    text_in_progress:{
      margin: "5%",
      fontFamily: "AktivGrotesk-Bold",
      fontSize: 20,
      color: Colors.black
    },
    text_success:{
      textAlign:'center',
      marginStart: "5%",
      marginEnd: "5%",
      marginBottom: "10%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 13,
      color: Colors.black
    },
    text_callon:{
      textAlign:'center',
      marginStart: "5%",
      marginEnd: "5%",
      marginBottom: "3%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 15,
      color: Colors.black
    },
    text_mobile:{
      padding:5,
      textAlign:'center',
      marginStart: "5%",
      marginEnd: "5%",
      marginBottom: "10%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 15,
      borderColor:'#D3d3d3',
      borderWidth:1,
      borderRadius:4,
      color: Colors.black
    },
    view_button:{
      width: "90%",
      
      
      marginStart: 20,
      marginEnd: 20
    },
    view_pin:{
      borderRadius: 4,
      height: 50,
      width: 50,
      backgroundColor: "#3444D6",
      justifyContent: "center",
      alignItems: "center"
    },
    text_pin:{
      color: Colors.white,
      fontSize: 30,
      fontFamily: "AktivGrotesk-Regular",
      textAlign: "center"
    },
    text_retry:{
      marginStart: "5%",
      marginEnd: "5%",
      fontFamily: "AktivGrotesk-Regular",
      fontSize: 11,
      color: Colors.black,
      textAlign: "center"
    }
  });

  export default PhoneNumberVerifyScreenStyle;